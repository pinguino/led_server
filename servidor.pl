#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  servidor.pl
#
#        USAGE:  ./servidor.pl  
#
#  DESCRIPTION: Servidor web para hacer demos de encendido/apagado de los leds
#               conectados en los pines 0, 4 y 6 de un dispisitivo Pingüino
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#      LICENCE: This library is free software, you can redistribute it and/or
#               modify it under the same terms as Perl itself.
#       AUTHOR:  Nelo R. Tovar (), tovar.nelo@gmail.com
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  21/09/11 17:09:34
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

{

    package WebServer;

    use HTTP::Server::Simple::CGI;
    use base qw(HTTP::Server::Simple::CGI);
    use Device::USB;

    my %dispatch = ('/'         => \&resp_index,
                    '/acerca'   => \&resp_acerca,
                    '/conmutar' => \&resp_conmutar,);

    my $usb = Device::USB->new() || die "No se puede inicializar el puerto USB!";
    my $dev = $usb->find_device(0x04d8, 0xfeaa) || die "Pingüino no está conectado!";
    $dev->open() || die "No se puede abrir la conexión con Pingüino:\n$!";
    $dev->set_configuration(3);
    $dev->claim_interface(0);

    my %num_led = ( ROJO => '0', AMARILLO => '2', VERDE => '4');

    sub handle_request {
        my $self = shift;
        my $cgi  = shift;

        my $path    = $cgi->path_info();
        my $handler = $dispatch{$path};

        if (ref($handler) eq "CODE") {
            print "HTTP/1.0 200 OK\r\n";
            $handler->($cgi);

        } else {
            print "HTTP/1.0 404 Not found\r\n";
            print $cgi->header,
              $cgi->start_html('Conmutar - P&aacute;gina no encontrada'),
              $cgi->h1('P&aacute;gina no encontrada'),
              $cgi->end_html;
        }
    }

    sub resp_index {
        my $cgi = shift;    # CGI.pm object
        return if !ref $cgi;

        print $cgi->header,
          $cgi->start_html("Conmutar - Inicio"),
          $cgi->h1("Demo para encender Led con Ping&uuml;ino");
        print $cgi->start_form(-method => 'POST', -action => 'conmutar');
        print $cgi->submit(-name => 'led', -value => 'Rojo'),     $cgi->br;
        print $cgi->submit(-name => 'led', -value => 'Amarillo'), $cgi->br;
        print $cgi->submit(-name => 'led', -value => 'Verde'),    $cgi->br;
        print $cgi->end_form;
        print $cgi->end_html;
    }

    sub resp_acerca {
        my $cgi = shift;    # CGI.pm object
        return if !ref $cgi;

        print $cgi->header,
          $cgi->start_html("Conmutar"),
          $cgi->h1("Conmutar"),
          $cgi->br,
          "Demo para conmutar led con Ping&uuml;ino",
          $cgi->br,
          "Por Nelo R. Tovar para Ping&uuml;ino-Ve",
          $cgi->br,
          $cgi->a({-href => '/'}, "Regresar"),
          $cgi->end_html;
    }

    sub resp_conmutar {
        my $cgi = shift;    # CGI.pm object
        return if !ref $cgi;

        my $led = uc($cgi->param('led'));

        $dev->bulk_write(0x01, $num_led{$led}, 100);

        print $cgi->header,
          $cgi->start_html("Conmutar $led"),
          $cgi->h1("Conmutar Led de Color $led"),
          $cgi->a({-href => '/'}, "Regresar"),
          $cgi->end_html;
    }

}

# start the server on port 8080
my $pid = WebServer->new(8080)->run();
print "Use 'kill $pid' para detener el servidor.\n";

