// conmutar.pde
// Leer del USB y conmutar un led
// Autor: Nelo R. Tovar
// Distribuido bajo licencia GPL-2
#define ROJO 0			// Led Rojo
#define AMARILLO 2	// Led Rojo
#define VERDE 4		// Led Rojo

uchar accion;

void conmutar_led(int led) {
    digitalWrite(led, !digitalRead(led));
}

void setup(void) {
  pinMode(ROJO, OUTPUT);
  digitalWrite(ROJO, LOW);
  pinMode(AMARILLO, OUTPUT);
  digitalWrite(AMARILLO, LOW);
  pinMode(VERDE, OUTPUT);
  digitalWrite(VERDE, LOW);
}

void loop(void) {
  if (USB.available()) {	// ¿Hay datos en el buffer del USB?
    accion = USB.read();	// Leer el datos enviado
    switch (accion) {		// ejecutar la acción
	   case '0':
        conmutar_led(ROJO);
		  break;
	   case '2':
        conmutar_led(AMARILLO);
		  break;
	   case '4':
        conmutar_led(VERDE);
		  break;
	 }
  }
}
